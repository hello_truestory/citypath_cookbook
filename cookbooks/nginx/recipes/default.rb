#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2013, True Story
#
# All rights reserved - Do Not Redistribute
#

package "nginx"

service "nginx" do
	supports :status => true, :restart => true, :reload => true
	action [:enable, :start]
end

template "/etc/nginx/nginx.conf" do
  	notifies :reload, "service[nginx]"
end
