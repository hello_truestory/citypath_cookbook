default[:nginx][:dir] 				 = "/etc/nginx"
default[:nginx][:default_user]       = "www-data"
default[:nginx][:worker_processes]   = 4
default[:nginx][:worker_connections] = 1024