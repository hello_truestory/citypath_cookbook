#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright 2013, True Story
#
# All rights reserved - Do Not Redistribute
#

include_recipe "apt"

package "php5-fpm"

service "php5-fpm" do
	supports :status => true, :restart => true, :reload => true
	action [:enable, :start]
end

template "/etc/php5/fpm/php.ini" do
  	notifies :reload, "service[php5-fpm]"
end

directory "/var/run/php5-fpm" do
	owner "root"
	group "root"
	mode 0755
end

package "php-pear"
package "php5-suhosin"
package "php5-xdebug"
package "php5-cli"
package "php5-curl"
package "php5-apc"
package "php5-gd"
package "php5-mysql"