#
# Cookbook Name:: app-php
# Recipe:: default
#
# Copyright 2013, True Story
#
# All rights reserved - Do Not Redistribute
#

include_recipe "nginx"
include_recipe "php"

common = {:name => "citypath", :app_root => "/var/www/"}

directory common[:app_root] do
	owner "root"
	group "root"
	mode 0755
end

user "citypath" do
  home common[:app_root]
  shell "/bin/false"
end

directory common[:app_root]+common[:name] do 
	owner "citypath"
  	group "citypath"
	mode 0755
end

%w(www booking).each do |dir|
	directory common[:app_root]+common[:name]+"/#{dir}" do 
		recursive true
		owner "citypath"
  		group "citypath"
		mode 0755
	end
	%w(log public).each do |subdir|
		directory common[:app_root]+common[:name]+"/#{dir}/#{subdir}" do 
			recursive true
			owner "citypath"
  			group "citypath"
			mode 0755
		end
	end
end

nginx_config_path_www     = "/etc/nginx/sites-available/citypath_www.conf"
nginx_config_path_booking = "/etc/nginx/sites-available/citypath_booking.conf"

template nginx_config_path_www do
	mode 0644
	source "citypath_www.conf.erb"
	variables common.merge({
		:server_names => "citypath.be www.citypath.be *.citypath.be", 
		:server_port => 80
	})
	notifies :reload, "service[nginx]"
end

template nginx_config_path_booking do
	mode 0644
	source "citypath_booking.conf.erb"
	variables common.merge({
		:server_names => "booking.citypath.be", 
		:server_port => 80
	})
	notifies :reload, "service[nginx]"
end

#enable virtual hosts
nginx_site "citypath_www" do
	config_path nginx_config_path_www
	action :enable
end

nginx_site "citypath_booking" do
	config_path nginx_config_path_booking
	action :enable
end

